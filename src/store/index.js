import Vuex from "vuex";

import state from "./state";
import actions from "./actions";
import mutations from "./mutations";

export default new Vuex.Store({
  actions,
  mutations,
  state
});
