const mongodb = require("mongodb");
const MongoClient = mongodb.MongoClient;
const _ = require("lodash");
const moment = require("moment");
const url = "mongodb://localhost:27017/feedbackly-demo";

const CATEGORIES = ["IPAD", "IPHONE", "GALAXY", "ONEPLUS", "SURFACE", "OTHER"];
const COUNTRIES = [
  { name: "Finland", region: "Europe" },
  { name: "Sweden", region: "Europe" },
  { name: "United Kingdom", region: "Europe" },
  { name: "Germany", region: "Europe" },
  { name: "Tokyo", region: "Asia" },
  { name: "Singapore", region: "Asia" },
  { name: "Mexico", region: "America" },
  { name: "Colombia", region: "America" }
];

async function start() {
  try {
    const db = await MongoClient.connect(url);
    const objects = [];

    for (i of new Array(1000)) {
      const insertObject = {
        category: _.sample(CATEGORIES),
        revenue: _.random(300, 1600),
        ts: moment()
          .subtract(_.random(0, 30), "days")
          .subtract(_.random(0, 86400), "seconds")
          .toDate()
      };

      const country = _.sample(COUNTRIES);
      insertObject.country = country.name;
      insertObject.region = country.region;

      objects.push(insertObject);
    }

    const result = await db.collection("transactions").insertMany(objects);
    console.log("Database seeded succesfully.");
    process.exit(0);
  } catch (err) {
    console.log(err);
  }
}

start();
